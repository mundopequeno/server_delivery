<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// entregadores
Route::post('deliveryman/auth', 'DeliverymanController@login');
Route::get('/deliveryman', 'DeliverymanController@showAll');

Route::middleware('authentication.deliveryman')
    ->prefix('deliveryman')
    ->group(function () {
        Route::post('', 'DeliverymanController@create');
        Route::post('/{id}/contact', 'DeliverymanController@addContact');
        Route::post('/{id}/address', 'DeliverymanController@addAddress');
        Route::post('/{id}/vehicle', 'DeliverymanController@addVehicle');
        // orders
        Route::get('/{id}/orders', 'DeliverymanController@getValidOrders');
        Route::post('/{id}/orders/{orderId}/accept', 'DeliverymanController@takeOrder');
        Route::post('/{id}/orders/{orderId}/delivered', 'DeliverymanController@delivered');
        Route::post('/{id}/orders/{orderId}/cancel', 'DeliverymanController@cancelOrder');
    });

// todo - entregador entrega

// estabelecimentos - rotas publicas
Route::post('establishment', 'EstablishmentController@create');
Route::get('establishment', 'EstablishmentController@show');
Route::post('establishment/auth', 'EstablishmentController@login'); // autenticação do estabelecimento gera um token válido
// rotas privadas
Route::middleware(['authentication.establishment'])
    ->prefix('establishment')
    ->group(function () {
        Route::get('/{establishmentId}/{distance}/{groupItemsQuantity}/{vehicleRequired}/', 'OrdersController@calculeShippingPrice'); // calcula frete
        Route::post('{id}/edit', 'EstablishmentController@edit');
        Route::post('/{id}/address', 'EstablishmentController@addAddress');
        Route::post('/{id}/contact', 'EstablishmentController@addContact');
        Route::post('/{id}/client', 'EstablishmentController@addClient');
        // pedidos
        Route::get('/{id}/orders', 'OrdersController@showAll');
        Route::post('/{id}/orders', 'OrdersController@create');
        Route::post('/{establishmentId}/orders/{orderId}/pay', 'OrdersController@pay');
        Route::post('/{id}/orders/{orderId}/releaseDelivery', 'EstablishmentController@confirmOutput');
        Route::post('/{id}/orders/{orderId}/removeDeliveryman', 'EstablishmentController@removeDeliveryman');
        Route::post('/{id}/orders/{orderId}/orderFinished', 'EstablishmentController@orderDone');
        Route::post('/{id}/orders/{orderId}/noDelivered', 'EstablishmentController@noDelivered');
    });

// veiculos permitidos
Route::post('/vehicleallowed', 'VechiclesAllowedController@create');

// modo de confirmação de entrega
Route::post('/delivery-confirmation-mode', 'DeliveryConfirmationModeController@create');