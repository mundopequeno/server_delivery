<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Contact extends Moloquent
{
    const TABLE_NAME = "contacts";
    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    protected $collection = self::TABLE_NAME;

    protected $fillable = [
        'celular', 'whatsapp', 'minimo_horario_receber_chamada', 'maximo_horario_receber_chamada'
    ];

    /**
     * Construir contado
     * @param string $celular
     * @param boolean $whatsapp
     * @param string $minimo_horario_receber_chamada
     * @param string $maximo_horario_receber_chamada
     */
    public function build ($belongsToId, $celular, $whatsapp, $minimo_horario_receber_chamada, $maximo_horario_receber_chamada, $status) {
        $this->belongsToId = $belongsToId;
        $this->celular = $celular;
        $this->whatsapp = $whatsapp;
        $this->minimo_horario_receber_chamada = $minimo_horario_receber_chamada;
        $this->maximo_horario_receber_chamada = $maximo_horario_receber_chamada;
        $this->status = $status;
    }
}
