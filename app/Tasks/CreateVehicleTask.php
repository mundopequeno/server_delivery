<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 21/01/2018
 * Time: 11:52
 */

namespace App\Tasks;


use App\Interfaces\Builder;
use App\Vehicle;

class CreateVehicleTask implements Builder
{
    /**
     * @var Vehicle
     */
    public $data;

    public function __construct($belongsToId, $objectWithValues, $status)
    {
        $vehicle = new Vehicle();

        $vehicle->build(
            $belongsToId,
            $objectWithValues->veiculo,
            $objectWithValues->cnh,
            $objectWithValues->placa,
            $objectWithValues->cor,
            $objectWithValues->ano,
            $objectWithValues->allowVehicleId,
            $status
        );

        $this->data = $vehicle;
    }

    public function save()
    {
        return $this->data->save();
    }
}