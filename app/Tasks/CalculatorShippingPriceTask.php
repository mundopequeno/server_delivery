<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 01/03/2018
 * Time: 18:41
 */

namespace App\Tasks;

use App\Entities\OrderRules;
use App\Establishment;
class CalculatorShippingPriceTask
{
    private $establishment;
    private $distance;
    private $quantity;
    private $vehicleRequired;
    private $shipprinPrice;

    /**
     * CalculatorShippingPriceTask constructor.
     * @param $establishment
     * @param $distance
     * @param $quantity
     * @param $vehicleRequired
     */
    public function __construct(Establishment $establishment, $distance, $quantity, $vehicleRequired)
    {
        // receber o estabelecimento futuramente vai servir para aplicar regras de porcentagem para o sistema
        // personalizada para cada cliente a partir de negociações
        $this->establishment = $establishment;
        $this->distance = $distance;
        $this->quantity = $quantity;
        $this->vehicleRequired = $vehicleRequired;
    }

    public function calcule () {
        // soma tudoooo!
        $distancePrice = OrderRules::getDistancePrice($this->distance);
        $priceByItems = OrderRules::getPriceByItems($this->quantity);
        $priceVehicleRequired = OrderRules::VEHICLE_RULES[$this->vehicleRequired];
        $plataformPercentage = OrderRules::PLATAFORM_PERCENTAGE;
        $minimumPrice = OrderRules::getMinimumPrice($this->vehicleRequired);

        $total = $distancePrice + $priceByItems + $priceVehicleRequired + $minimumPrice;

        // calcula a porcentagem baseado no valor total
        $total = $total + ($total * $plataformPercentage);

        return $total;
    }
}