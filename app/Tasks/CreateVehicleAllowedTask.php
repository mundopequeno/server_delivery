<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 26/01/2018
 * Time: 14:24
 */

namespace App\Tasks;


use App\Interfaces\Builder;
use App\VehicleAllowed;

class CreateVehicleAllowedTask implements Builder
{
    /**
     * @var VehicleAllowed
     */
    public $data;

    public function __construct($belongsToId = null, $objectWithValues, $status)
    {
        $vehicleAllowed = new VehicleAllowed();

        $vehicleAllowed->build(
            $objectWithValues->name,
            $objectWithValues->nameValidate,
            $objectWithValues->description,
            $status
        );

        $this->data = $vehicleAllowed;
    }

    public function save()
    {
        return $this->data->save();
    }
}