<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 25/01/2018
 * Time: 02:05
 */

namespace App\Tasks;


use App\Client;
use App\Interfaces\Builder;

class CreateClientTask implements Builder
{
    /**
     * @var Client
     */
    public $data;


    /**
     * CreateClientTask constructor.
     * @param string $belongsToId
     * @param object $objectWithValues
     * @param null $status
     */
    public function __construct($belongsToId, $objectWithValues, $status = null)
    {
        $client = new Client();

        $client->build(
            $belongsToId,
            $objectWithValues->nome,
            $objectWithValues->cpf,
            Client::STATUS_ON
        );

        $this->data = $client;
    }

    public function save()
    {
        return $this->data->save();
    }
}