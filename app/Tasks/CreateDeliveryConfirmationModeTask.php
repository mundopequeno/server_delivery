<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 26/01/2018
 * Time: 14:58
 */

namespace App\Tasks;


use App\DeliveryConfirmationMode;
use App\Interfaces\Builder;

class CreateDeliveryConfirmationModeTask implements Builder
{
    /**
     * @var DeliveryConfirmationMode
     */
    public $data;

    public function __construct($belongsToId = null, $objectWithValues, $status)
    {
        $deliveryConfirmationMode = new DeliveryConfirmationMode();

        $deliveryConfirmationMode->build(
            $objectWithValues->name,
            $objectWithValues->nameValidate,
            $objectWithValues->description,
            $status
        );

        $this->data = $deliveryConfirmationMode;
    }

    public function save()
    {
        $this->data->save();
    }
}