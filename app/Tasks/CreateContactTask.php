<?php
namespace App\Tasks;

use App\Contact;
use App\Interfaces\Builder;

/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 20/01/2018
 * Time: 03:26
 */
class CreateContactTask implements Builder
{
    /**
     * @var Contact
     */
    public $data;

    /**
     * CreateContactTask constructor.
     * @param string $belongsToId
     * @param object|array $objectWithValues
     * @param int $status
     */
    public function __construct($belongsToId, $objectWithValues, $status)
    {
        $contact = new Contact();

        $contact->build(
            $belongsToId,
            $objectWithValues->celular,
            $objectWithValues->whatsapp,
            $objectWithValues->minimo_horario_receber_chamada,
            $objectWithValues->maximo_horario_receber_chamada,
            $status
        );

        $this->data = $contact;
    }

    /**
     * Retorna dados do contato salvo
     * @return string
     */
    public function save() {
        return $this->data->save();
    }
}