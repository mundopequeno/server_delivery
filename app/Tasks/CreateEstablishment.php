<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/01/2018
 * Time: 00:26
 */

namespace App\Tasks;


use App\Establishment;
use App\Interfaces\Builder;

class CreateEstablishment implements Builder
{
    /**
     * @var Establishment
     */
    public $data;

    public function __construct($belongsToId = null, $objectWithValues, $status = null)
    {
        $establishment = new Establishment();

        $establishment->build(
            $objectWithValues->nome,
            $objectWithValues->cnpj,
            $objectWithValues->email,
            $objectWithValues->senha,
            $objectWithValues->tipo,
            $objectWithValues->horario_aberto,
            $objectWithValues->horario_fechado,
            Establishment::STATUS_MISSING_DATA
        );

        $this->data = $establishment;
    }

    public function save()
    {
        return $this->data->save();
    }
}