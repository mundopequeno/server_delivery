<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 20/01/2018
 * Time: 04:12
 */

namespace App\Tasks;


use App\Address;
use App\Interfaces\Builder;

class CreateAddressTask implements Builder
{
    /**
     * @var Address
     */
    public $data;

    /**
     * CreateAddressTask constructor.
     * @param string $belongsToId
     * @param array|object $objectWithValues
     * @param int $status
     * @throws \Exception
     */
    public function __construct($belongsToId, $objectWithValues, $status)
    {
        if (!in_array($objectWithValues->type_residence, Address::ALLOW_RESIDENCES))
            throw new \Exception("Tipo de residencia não permitido", 400);

        $address = new Address();

        $address->build(
            $belongsToId,
            $objectWithValues->cep,
            $objectWithValues->number,
            $objectWithValues->street,
            $objectWithValues->neighborhood,
            $objectWithValues->city,
            $objectWithValues->state,
            $objectWithValues->complement,
            $objectWithValues->reference,
            $objectWithValues->latitude,
            $objectWithValues->longitude,
            $objectWithValues->type_residence,
            $status
        );

        $this->data = $address;
    }

    public function save()
    {
        return $this->data->save();
    }
}