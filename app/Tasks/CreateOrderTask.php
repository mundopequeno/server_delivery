<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 25/01/2018
 * Time: 00:40
 */

namespace App\Tasks;


use App\Client;
use App\Entities\GroupItems;
use App\Interfaces\Builder;
use App\Tasks\CreateAddressTask;
use App\Tools\ValidatorTools;
use App\Item;
use App\Order;
use App\Address;
use App\Entities\Address as AddressEntitie;
use App\Entities\Item as ItemEntitie;

class CreateOrderTask implements Builder
{
    public $data;

    public function __construct($belongsToId, $objectWithValues, $status)
    {
        $order = new Order();

        $this->validateObjectWithValues($objectWithValues);

        $order->establishmentId = $belongsToId;
        $order->client = $this->getClientData($objectWithValues->clientId);
        $order->shippingAddress = $this->getShippingAddress($objectWithValues->shippingAddress);
        $order->deliveryManId = $objectWithValues->deliveryManId;
        $order->items = $this->setItems($objectWithValues->items);
        $order->price = $this->getShippingPrice($objectWithValues->price);
        $order->status = $status;

        $this->data = $order;
    }

    private function getClientData ($clientId) {
        return isset($clientId) && !empty($clientId) ? Client::find($clientId) : null;
    }

    private function setItems ($jsonItems) {
        $groupItems = json_decode($jsonItems);

        /**
         * @var GroupItems[] $listGroups
         */
        $listGroups = [];
        $extractListItems  = [];

        // montagem de grupos
        foreach ($groupItems as $key => $item) {
            if (!isset($item->type)) throw new \Exception("Erro na estrutura dos dados", 400);
            if (!isset($item->listItems) && empty($item->listItems)) throw new \Exception("Lista de itens da {$item->type} está vazio", 400);

            $extractListItems[] = ['list' => $item->listItems, 'groupIndex' => $key];
            $listGroups[] = new GroupItems($item->type);
        }

        // inserção de items nos grupos
        foreach ($extractListItems as $item) {
            foreach ($item['list'] as $list) {
                if (!isset($list->name)) throw new \Exception("Nome do item não localizado", 400);
                if (!isset($list->quantity)) throw new \Exception("Quantidade do item não localizada", 400);

                $listGroups[$item['groupIndex']]->addItem(new ItemEntitie($list->name, $list->quantity));
            }
        }

        $groups = [];
        // mais um for para poder reescrever os itens para string ao invés de deixa-los em objeto
        foreach ($listGroups as $group) $groups[] = $group->getAllAttributes();

        return $groups;
    }

    private function getShippingPrice ($price) {
        if (empty($price)) throw new \Exception("O valor deve ser calculado.", 400);
        return $price;
    }

    private function getShippingAddress ($jsonShippingAddress) {
        $shippingAddress = json_decode($jsonShippingAddress);

        // cria entidade endereço
        $addressEntitie = new AddressEntitie();
        $addressEntitie->setCep($shippingAddress->cep);
        $addressEntitie->setNumber($shippingAddress->number);
        $addressEntitie->setStreet(isset($shippingAddress->street) ? $shippingAddress->street : null);
        $addressEntitie->setNeighborhood(isset($shippingAddress->neighborhood) ? $shippingAddress->neighborhood : null);
        $addressEntitie->setCity(isset($shippingAddress->city) ? $shippingAddress->city : null);
        $addressEntitie->setState(isset($shippingAddress->state) ? $shippingAddress->state : null);
        $addressEntitie->setComplement(isset($shippingAddress->complement) ? $shippingAddress->complement : null);
        $addressEntitie->setReference(isset($shippingAddress->reference) ? $shippingAddress->reference : null);
        $addressEntitie->setLatitude(isset($shippingAddress->latitude) ? $shippingAddress->latitude : null);
        $addressEntitie->setLongitude(isset($shippingAddress->longitude) ? $shippingAddress->longitude : null);
        $addressEntitie->setTypeResidence(isset($shippingAddress->type_residence) ? $shippingAddress->type_residence : null);

        return $addressEntitie->getAllAttributes();
    }

    private function validateObjectWithValues($objectWithValues) {
        if (empty($objectWithValues->clientId) && empty($objectWithValues->shippingAddress))
            throw new \Exception('É necessário informar o client ou o endereço de entrega.', 400);
    }

    public function save() {
        return $this->data->save();
    }
}