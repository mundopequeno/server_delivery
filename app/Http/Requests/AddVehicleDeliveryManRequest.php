<?php

namespace App\Http\Requests;

use App\Vehicle;
use App\VehicleAllowed;
use Illuminate\Foundation\Http\FormRequest;

class AddVehicleDeliveryManRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            "cnh" => "required|regex:/^([\\d]{3})([\\d]{3})([\\d]{4})([\\d]{1})$/",
            "placa" => "required|regex:/^\\w{3}\\-\\d{4}$/",
            'renavam' => 'required|regex:/(\\d{4})[.](\\d{6})-(\\d{1})/',
            'cor' => 'required',
            'modelo' => 'required',
//            "ano" => "required|regex:/^\\d{4}/",
//            "veiculo" => "required"
        ];
    }
}
