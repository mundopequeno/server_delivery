<?php

namespace App\Http\Requests;

use App\VehicleAllowed;
use Illuminate\Foundation\Http\FormRequest;

class CreateVehicleAllowedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $vehicleAllowedTableName = VehicleAllowed::TABLE_NAME;

        return [
            "name" => "required|min:2",
            "nameValidate" => "required|min:2|unique:{$vehicleAllowedTableName}",
            "description" => "",
            "status" => ""
        ];
    }
}
