<?php

namespace App\Http\Requests;

use App\Contact;
use App\DeliveryMan;
use Illuminate\Foundation\Http\FormRequest;


class AddContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "celular" => "required|regex:/^\\(\\d{2}\\)\\d{4,5}\\-\\d{4}$/",
            "whatsapp" => "required|boolean",
            "minimo_horario_receber_chamada" => "required|regex:/^\\d{1,2}\\:\\d{2}$/",
            "maximo_horario_receber_chamada" => "required|regex:/^\\d{1,2}\\:\\d{2}$/"
        ];
    }
}
