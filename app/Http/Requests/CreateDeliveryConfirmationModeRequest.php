<?php

namespace App\Http\Requests;

use App\DeliveryConfirmationMode;
use Illuminate\Foundation\Http\FormRequest;

class CreateDeliveryConfirmationModeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $deliveryConfirmationModeTableName = DeliveryConfirmationMode::TABLE_NAME;

        return [
            "name" => "required|min:2",
            "nameValidate" => "required|min:2|unique:{$deliveryConfirmationModeTableName}",
            "description" => "",
            "status" => ""
        ];
    }
}
