<?php

namespace App\Http\Requests;

use App\DeliveryMan;
use Illuminate\Foundation\Http\FormRequest;

class CreateDeliveryManRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // pego nome da table do entregador
        $tableNameDeliveryMan = DeliveryMan::TABLE_NAME;

        return [
            "nome" => "required|string|min:3",
            "sobrenome" => "required|string|min:3",
            "rg" => "regex:/^\\d{2}\\.\\d{3}\\.\\d{3}\\-\\d+/|unique:{$tableNameDeliveryMan}",
            "cpf" => "regex:/^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$/|unique:{$tableNameDeliveryMan}",
            "dataNascimento" => "required|date",
            "email" => "required|email",
            "foto" => "image",
            "senha" => "required|min:4|confirmed"
        ];
    }
}
