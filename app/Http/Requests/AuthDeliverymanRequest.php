<?php

namespace App\Http\Requests;

use App\DeliveryMan;
use Illuminate\Foundation\Http\FormRequest;

class AuthDeliverymanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $deliverymanTableName = DeliveryMan::TABLE_NAME;

        return [
            'email' => "required|email|exists:{$deliverymanTableName}",
            'senha' => 'required'
        ];
    }
}
