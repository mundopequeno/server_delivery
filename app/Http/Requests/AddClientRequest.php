<?php

namespace App\Http\Requests;

use App\Client;
use Illuminate\Foundation\Http\FormRequest;

class AddClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $clientTableName = Client::TABLE_NAME;

        return [
            "nome" => "min:2",
            "cpf" => "regex:/^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$/|unique:{$clientTableName}"
        ];
    }
}
