<?php

namespace App\Http\Requests;

use App\Establishment;
use Illuminate\Foundation\Http\FormRequest;

class CreateEstablishmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $establishmentTableName = Establishment::TABLE_NAME;

        return [
            "nome" => "required|min:1",
            "cnpj" => "required|regex:/^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$/|unique:{$establishmentTableName}",
            "email" => "required|email|unique:{$establishmentTableName}",
            "senha" => "required|min:4|confirmed",
            "tipo" => "required",
            "horario_aberto" => "required|regex:/\\d{2}\\:\\d{2}/",
            "horario_fechado" => "required|regex:/\\d{2}\\:\\d{2}/"
        ];
    }
}
