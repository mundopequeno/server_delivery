<?php

namespace App\Http\Requests;

use App\Client;
use App\DeliveryConfirmationMode;
use App\DeliveryMan;
use App\Establishment;
use App\VehicleAllowed;
use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $deliveryManTableName = DeliveryMan::TABLE_NAME;
        $clientTableName = Client::TABLE_NAME;

        return [
            "clientId" => "exists:{$clientTableName},_id",
            'shippingAddress' => 'json',
            "deliveryManId" => "exists:{$deliveryManTableName},_id", // caso já tiver um entregador fixo
            "items" => "required|json",
            "price" => "required|regex:/^\\d+\\.\\d{2}$/"
        ];
    }
}
