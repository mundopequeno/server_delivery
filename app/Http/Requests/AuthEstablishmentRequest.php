<?php

namespace App\Http\Requests;

use App\Establishment;
use Illuminate\Foundation\Http\FormRequest;

class AuthEstablishmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $establishmentTableName = Establishment::TABLE_NAME;

        return [
            'email' => "required|email|exists:{$establishmentTableName}",
            'senha' => 'required'
        ];
    }
}
