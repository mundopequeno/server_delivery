<?php

namespace App\Http\Requests;

use App\Establishment;
use Illuminate\Foundation\Http\FormRequest;

class EditEstablishmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $establishmentTableName = Establishment::TABLE_NAME;

        return [
            "nome" => "min:1",
            "cnpj" => "regex:/^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$/|unique:{$establishmentTableName}",
            "email" => "email|unique:{$establishmentTableName}",
            "senha" => "min:4|confirmed",
            "tipo" => "",
            "horario_aberto" => "regex:/\\d{2}\\:\\d{2}/",
            "horario_fechado" => "regex:/\\d{2}\\:\\d{2}/"
        ];
    }
}
