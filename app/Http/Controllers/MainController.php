<?php

namespace App\Http\Controllers;

use App\Contact;
use App\DeliveryMan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function test (Request $request) {
//        $contact = new Contact();
//        $contact->phone = "11995573595";
//        $contact->email = "pedro@email.com";
//        $contact->status = 1;
//        $contact->save();


        return view('test')->with("contact", DeliveryMan::all());
    }
}
