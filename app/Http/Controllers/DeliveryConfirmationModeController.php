<?php

namespace App\Http\Controllers;

use App\DeliveryConfirmationMode;
use App\Http\Requests\CreateDeliveryConfirmationModeRequest;
use App\Tasks\CreateDeliveryConfirmationModeTask;
use Illuminate\Http\Request;

class DeliveryConfirmationModeController extends Controller
{
    public function create (CreateDeliveryConfirmationModeRequest $request) {
        $deliveryConfirmationMode = new CreateDeliveryConfirmationModeTask(null, $request, DeliveryConfirmationMode::STATUS_ON);
        $deliveryConfirmationMode->save();

        return response()->json($deliveryConfirmationMode->data, 201);
    }
}
