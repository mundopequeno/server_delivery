<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVehicleAllowedRequest;
use App\Tasks\CreateVehicleAllowedTask;
use App\VehicleAllowed;
use Illuminate\Http\Request;

class VechiclesAllowedController extends Controller
{
    public function create (CreateVehicleAllowedRequest $request) {
        $vehicleAllowed = new CreateVehicleAllowedTask(null, $request, VehicleAllowed::STATUS_ON);
        $vehicleAllowed->save();

        return response()->json($vehicleAllowed->data, 201);
    }
}
