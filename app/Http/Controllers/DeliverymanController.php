<?php

namespace App\Http\Controllers;

use App\Address;
use App\Builders\QueryBuilder;
use App\Contact;
use App\DeliveryMan;
use App\Token;
use App\Http\Requests\AddAddressRequest;
use App\Http\Requests\AddContactRequest;
use App\Http\Requests\AddVehicleDeliveryManRequest;
use App\Http\Requests\AuthDeliverymanRequest;
use App\Http\Requests\CreateDeliveryManRequest;
use App\Http\Requests\TakeOrderRequest;
use App\Order;
use App\Tasks\CreateAddressTask;
use App\Tasks\CreateContactTask;
use App\Tasks\CreateVehicleTask;
use App\Vehicle;
use Illuminate\Http\Request;

class DeliverymanController extends Controller
{
    /**
     * @param AuthDeliverymanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login (AuthDeliverymanRequest $request) {
        try {

            // busca o entregador
            $deliveryman = DeliveryMan::with(['token'])
                ->where('email', $request->email)
                ->first();

            // valida credenciais
            if (empty($deliveryman)) throw new \Exception("Estabelecimento não encontrado", 400);
            if ($request->senha !== decrypt($deliveryman->senha)) throw new \Exception("Senha incorreta", 400);


            // valida se o deliveryman está ativado
            if ($deliveryman->status == DeliveryMan::STATUS_OFF) throw new \Exception("Sua conta está temporariamente desativada", 401);

            // pego o token do estabelecimento
            $token = $deliveryman->token;

            // caso o token não existir
            if (empty($token)) {
                $token = new Token();
                $token->status = Token::AUTHENTICATION;
                $token->ownerId = $deliveryman->_id;
            }

            $token->generate($deliveryman); // gero token + updated_at
            $deliveryman->token()->save($token); // salvo o token / atualizo do estabelecimento

            return response()->json(['deliveryman' => $deliveryman->refresh()], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    // aux method from method create
    /**
     * @param Request $request
     * @param \App\Entities\Deliveryman $deliveryManEntitie
     * @return bool
     * @throws \Exception
     */
    private function updateDeliveryMan (Request $request, \App\Entities\Deliveryman $deliveryManEntitie) {
        /** @var DeliveryMan $deliveryManModel */
        $deliveryManModel = DeliveryMan::find($request->deliverymanId);
        $verificaEmail = DeliveryMan::where('email', $deliveryManEntitie->getEmail())->first();

        if (empty($deliveryManModel)) throw new \Exception("Entregador não encontrado", 404);

        $deliveryManEntitie->setRg($deliveryManModel->rg);
        $deliveryManEntitie->setCpf($deliveryManModel->cpf);

        // validar se há troca de e-mail
        if ($deliveryManModel->email !== $deliveryManEntitie->getEmail() && !empty($verificaEmail)) throw new \Exception("Este e-mail já esta sendo usado por outro usuário", 400);

        // validar se há troca de cpf
        if ($deliveryManModel->cpf !== $deliveryManEntitie->getCpf()) throw new \Exception("Não é permitido a troca de CPF, ligue para o suporte caso houver necessidade", 400);

        // valida se há troca de RG
        if ($deliveryManModel->rg !== $deliveryManEntitie->getRg()) throw new \Exception("Não é permitido a troca de RG, ligue para o suporte caso houver necessidade", 400);

        return $deliveryManModel->update($deliveryManEntitie->getAllAttributes());
    }

    /**
     * @param CreateDeliveryManRequest $request
     * @param \App\Entities\Deliveryman $deliveryManEntitie
     * @return bool
     * @throws \Exception
     */
    private function saveDeliveryMan (CreateDeliveryManRequest $request, \App\Entities\Deliveryman $deliveryManEntitie) {
        $deliveryManEntitie->setRg($request->input('rg'));
        $deliveryManEntitie->setCpf($request->cpf);

        $verificaEmail = DeliveryMan::where('email', $deliveryManEntitie->getEmail())->first();

        // validar se o e-mail informado é válido
        if (!empty($verificaEmail)) throw new \Exception("Este e-mail já esta sendo usado por outro usuário", 400);

        $deliveryManModel = new DeliveryMan($deliveryManEntitie->getAllAttributes());
        return $deliveryManModel->save();
    }

    /**
     * @param CreateDeliveryManRequest $request
     * @return DeliveryMan|\Illuminate\Http\JsonResponse
     */
    public function create (CreateDeliveryManRequest $request)
    {
        try {
            $deliveryMan = new \App\Entities\Deliveryman();
            $deliveryMan->setAttributesWithRequest($request);

            $deliveryMan->setStatus(DeliveryMan::STATUS_ON);

            $deliveryMan->setHasVehicle(DeliveryMan::HAS_NOT_VEHICLE);
            $deliveryMan->setHasAddress(DeliveryMan::HAS_NOT_ADDRESS);
            $deliveryMan->setHasContact(DeliveryMan::HAS_NOT_CONTACT);

            // add endereço
            if (!empty($request->cep)) {
                $address = new \App\Entities\Address();
                $address->setAttributesWithRequest($request);
                $deliveryMan->setAddress($address);
                $deliveryMan->setHasAddress(DeliveryMan::HAS_ADDRESS);
            }

            // add contact
            if (!empty($request->celular)) {
                $contact = new \App\Entities\Contact();
                $contact->setAttributesWithRequest($request);
                $deliveryMan->setContact($contact);
                $deliveryMan->setHasContact(DeliveryMan::HAS_CONTACT);
            }

            if (!empty($request->deliverymanId)) $this->updateDeliveryMan($request, $deliveryMan);
            else $this->saveDeliveryMan($request, $deliveryMan);

            $deliveryManModel = DeliveryMan::where('email', $deliveryMan->getEmail())
                ->with(['addresses', 'contacts', 'token', 'vehicle'])
                ->first();

            return response()->json(['deliveryman' => $deliveryManModel], 201);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }


        // todo - enviar e-mail desejando as boas vindas para o novo entregador e outras mensagens auxiliares como, como conseguir informações de procedimento, como entrar em contato e etc.
    }
    // end create

    /**
     * Mostra todos os entregadores
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAll (Request $request) {
        $deliveryMans = DeliveryMan::with(['contacts', 'addresses', 'vehicle'])
            ->get();

        return response()->json($deliveryMans, 200);
    }

    /**
     * @param AddContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addContact (AddContactRequest $request) {
        /** @var DeliveryMan $deliveryman */
        $deliveryman = DeliveryMan::find($request->id);

        if (empty($deliveryman)) return response()->json(["message" => "Entregador não encontrado"], 404);

        try {
            // build de modelo para entidade
            $deliverymanEntitie = new \App\Entities\Deliveryman();
            $deliverymanEntitie->setAttributesWithModel($deliveryman);

            // crio / atualizo o contato
            $contactEntitie = new \App\Entities\Contact();
            $contactEntitie->setAttributesWithRequest($request);
            $deliverymanEntitie->setContact($contactEntitie);

            $deliveryman->update($deliverymanEntitie->getAllAttributes());

            return response()->json(['deliveryman' => $deliveryman], 200);

        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @param AddAddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAddress (AddAddressRequest $request) {
        /** @var DeliveryMan $deliveryman */
        $deliveryman = DeliveryMan::find($request->id);

        if (empty($deliveryman)) return response()->json(["message" => "Entregador não encontrado"], 404);

        try {
            // build de modelo para entidade
            $deliverymanEntitie = new \App\Entities\Deliveryman();
            $deliverymanEntitie->setAttributesWithModel($deliveryman);

            // crio / atualizo o contato
            $addressEntitie = new \App\Entities\Address();
            $addressEntitie->setAttributesWithRequest($request);
            $deliverymanEntitie->setAddress($addressEntitie);

            $deliveryman->update($deliverymanEntitie->getAllAttributes());

            return response()->json(['deliveryman' => $deliveryman], 200);

        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @param AddVehicleDeliveryManRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addVehicle (AddVehicleDeliveryManRequest $request) {
        /** @var DeliveryMan $deliveryman */
        $deliveryman = DeliveryMan::find($request->id);

        if (empty($deliveryman)) return response()->json(["message" => "Entregador não encontrado"], 404);

        try {
            // build de modelo para entidade
            $deliverymanEntitie = new \App\Entities\Deliveryman();
            $deliverymanEntitie->setAttributesWithModel($deliveryman);

            // crio / atualizo o veiculo
            $vehicleEntitie = new \App\Entities\Vehicle();
            $vehicleEntitie->setAttributesWithRequest($request);
            $deliverymanEntitie->setVehicle($vehicleEntitie);

            $deliveryman->update($deliverymanEntitie->getAllAttributes());

            return response()->json(['deliveryman' => $deliveryman], 201);

        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Retorna as entregas validas para o entregador solicitante
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getValidOrders (Request $request) {

        try {
            $deliveryman = DeliveryMan::find($request->id);
            if (empty($deliveryman)) throw new \Exception("Entregador não encontrado", 404);

            // filtrar parametros
            $queryBuilder = new QueryBuilder($request, new Order());

            // if relations empty set all
//            if (empty($queryBuilder->relations)) $queryBuilder->relations = ['establishment', 'client', 'allowVehicle', 'modeConfirmation'];

            $queryBuilder->buildWhere("deliveryManId:null");

            $queryBuilder->build();

            return response()->json($queryBuilder->getAll(), 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Sai de uma entrega
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelOrder (Request $request) {
        try {
            $deliveryman = DeliveryMan::find($request->id);
            if (empty($deliveryman)) throw new \Exception("Entregador não encontrado", 404);

            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            // validar se o entregador é responsavel pela entrega do pedido informado
            if ($deliveryman->_id !== $order->deliveryManId) throw new \Exception("Você não está registrado neste pedido, caso isso parecer estranho entre em contato conosco.", 400);

            // validar status da entrega antes de deixar
            if ($order->status == Order::STATUS_ORDER_DELIVERED) throw new \Exception("Entrega já foi realizada.", 200);
            if ($order->status !== Order::STATUS_DELIVERYMAN_ACCEPT) throw new \Exception("Não é possível deixar a entrega, solicite esta ação ao estabelecimento.", 400);

            $order->deliveryManId = '';
            $order->status = Order::STATUS_PENDING;

            // todo - notificar o estabelecimento que o entregador deixou a entrega

            $order->save();

            return response()->json(['message' => 'Você deixou a entrega.'], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Este método deveria estar no COntroller do entregador
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function takeOrder (Request $request) {

        try {
            // pegar id do entregador e validar
            $deliveryMan = DeliveryMan::find($request->id);

            if (empty($deliveryMan)) throw new \Exception("Entregador não encontrado", 400);

            // pegar id do pedido e validar
            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            if (!empty($order->deliveryManId) && $order->deliveryManId == $deliveryMan->_id)
                throw new \Exception("Você já aceitou este pedido", 400);

            if (!empty($order->deliveryManId)) throw new \Exception("Um entregador já confirmou essa entrega", 400);

            // verificar status do pedido se ainda esta pendente
            if ($order->status == Order::STATUS_CANCELED) throw new \Exception("A entrega não está mais disponível", 400);
            if ($order->status == Order::STATUS_WAITING_PAYMENT) throw new \Exception("Desculpe, ainda não é possível aceitar está entrega. Tente novamente mais tarde", 400);
            if ($order->status !== Order::STATUS_PENDING) throw new \Exception("Entrega já em andamento", 400);

            // adicionar entregador no pedido e trocar status do pedido para entregador aceitou
            $order->deliveryManId = $deliveryMan->_id;
            $order->status = Order::STATUS_DELIVERYMAN_ACCEPT;
            $order->save();

            return response()->json(["message" => "Entrega aceita com sucesso, vá para o estabelecimento para pegar a mercadoria"], 200);

        }catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

    }

    /**
     * Declara que já realizou a entrega
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delivered (Request $request) {
        try {
            $deliveryman = DeliveryMan::find($request->id);
            if (empty($deliveryman)) throw new \Exception("Entregador não encontrado", 404);

            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            if ($deliveryman->_id !== $order->deliveryManId) throw new \Exception("Esta entrega não esta alocada para você.", 400);

            if ($order->status == Order::STATUS_ORDER_DELIVERED) throw new \Exception("Entrega já foi realizada.", 200);
            if ($order->status !== Order::STATUS_DELIVERYMAN_WAY) throw new \Exception("Este pedido não esta em condições de ser declarado como entregue ou já foi declarado.", 400);

            $order->status = Order::STATUS_DELIVERYMAN_CONFIRMED;
            $order->save();

            // todo - notificar o estabelecimento de que a entrega foi declarada como entregue

            return response()->json(['message' => 'A entrega foi marcada como já entregue, o estabelecimento irá verificar e confirmar.'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
 }
