<?php

namespace App\Http\Controllers;

use App\Builders\QueryBuilder;
use App\DeliveryMan;
use App\Entities\OrderRules;
use App\Establishment;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\TakeOrderRequest;
use App\Order;
use App\Tasks\CalculatorShippingPriceTask;
use App\Tasks\CreateOrderTask;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * @param CreateOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (CreateOrderRequest $request) {
        $establishment = Establishment::find($request->id);

        if (empty($establishment)) return response()->json(["message" => "Estabelecimento não foi encontrado"], 404);

        try {
            $order = new CreateOrderTask($establishment->_id, $request, Order::STATUS_WAITING_PAYMENT);
            $order->save();
            
            return response()->json($order->data, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function calculeShippingPrice (Request $request) {

        try {
            $establishment = Establishment::find($request->establishmentId);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            $distance = (int) $request->distance;
            $quantity = (int) $request->groupItemsQuantity;

            if (empty($distance)) throw new \Exception("Distancia zerada", 400);
            if (empty($quantity)) throw new \Exception("Quantidade zerada", 400);

            if (!key_exists($request->vehicleRequired, OrderRules::VEHICLE_RULES)) throw new \Exception("Tipo de veiculo não encontrado", 400);
            if (!is_int($distance)) throw new \Exception("Certifique-se de que está informando a distancia em metros", 400);
            if (!is_int($quantity)) throw new \Exception("Certifique-se de que está informando um valor inteiro", 400);

            $calculatorShippingPriceTask = new CalculatorShippingPriceTask($establishment, $distance, $quantity, $request->vehicleRequired);

            return response()->json(['price' => $calculatorShippingPriceTask->calcule()], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAll (Request $request) {
        $establishment = Establishment::find($request->id);

        if (empty($establishment)) return response()->json(["message" => "Estabelecimento não encontrado"], 404);

        // filtrar parametros
        $queryBuilder = new QueryBuilder($request, new Order());

        // if relations empty set all
        if (empty($queryBuilder->relations)) $queryBuilder->relations = ['establishment', 'client', 'allowVehicle', 'modeConfirmation'];

        $queryBuilder->build();
        $orders = $queryBuilder->getAll();

        return response()->json($orders, 200);
    }


    //    ESTABLISHMENT

    /**
     * [pay description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function pay (Request $request) {
        // IMPROTANTE: enviar dados do cartão bancário via post

        try {
            // valida o estabelecimento
            $establishment = Establishment::find($request->establishmentId);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            // valida se o pedido existe
            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não foi encontrado", 404);

            // valida se o pedido é do estabelecimento pertencente
            if ($establishment->_id !== $order->establishmentId) throw new \Exception("Este pedido não pertence ao seu estabelecimento", 400);

            // realiza pagamento
            
            // verifica status do pagamento
            
            // caso status aprovado troca status
            
            // caso status aguardando pagamento retornar que o pagamento esta em processamento
            
            // caso status recusado retornar mensagem informando
            
            // por agora o processo é gratuito
            $order->status = Order::STATUS_PENDING;
            $order->save();

            return response()->json(['message' => 'A entrega foi efetivada com sucesso e já esta sendo exibida para os entregadores.']);

        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
