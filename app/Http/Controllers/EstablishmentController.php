<?php

namespace App\Http\Controllers;

use App\Address;
use App\Builders\QueryBuilder;
use App\Client;
use App\Contact;
use App\DeliveryMan;
use App\Establishment;
use App\Http\Requests\AuthEstablishmentRequest;
use App\Http\Requests\AddAddressRequest;
use App\Http\Requests\AddClientRequest;
use App\Http\Requests\AddContactRequest;
use App\Http\Requests\CreateEstablishmentRequest;
use App\Http\Requests\EditEstablishmentRequest;
use App\Interfaces\QueryBuilderInterface;
use App\Order;
use App\Tasks\CreateAddressTask;
use App\Tasks\CreateClientTask;
use App\Tasks\CreateContactTask;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Entities\Address as AddressEntitie;
use App\Entities\Contact as ContactEntitie;
use App\Entities\Client as ClientEntitie;

class EstablishmentController extends Controller implements QueryBuilderInterface
{
    public function login (AuthEstablishmentRequest $request) {

        try {

            // busca o estabelecimento
            $establishment = Establishment::with(['addresses', 'contacts', 'token'])
                ->where('email', $request->email)
                ->first();

            // valida credenciais
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 400);
            if ($request->senha !== decrypt($establishment->senha)) throw new \Exception("Senha incorreta", 400);

            // valida se o estabelecimento está ativado
            if ($establishment->status == Establishment::STATUS_OFF) throw new \Exception("Seu estabelecimento está temporariamente desativado", 401);

            // pego o token do estabelecimento
            $token = $establishment->token;

            // caso o token não existir
            if (empty($token)) {
                $token = new Token();
                $token->status = Token::AUTHENTICATION;
                $token->ownerId = $establishment->_id;
            }

            $token->generate($establishment); // gero token + updated_at
            $establishment->token()->save($token); // salvo o token / atualizo do estabelecimento

            return response()->json(['establishment' => $establishment->refresh()], 200);
            
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 401);
        }
    }

    public function show (Request $request) {
        $queryBuilder = new QueryBuilder($request, new Establishment());
        $queryBuilder->build();
        return $queryBuilder->getAll();
    }

    public function create (CreateEstablishmentRequest $request) {
        $establishment = new \App\Tasks\CreateEstablishment(null, $request, null);
        $establishment->save();

        // habilitar token de confirmação de conta quando o sistema ficar bastante usado

//        $token = new Token();
//        $token->generate($establishment);
//        $token->ownerId = $establishment->_id;
//        $token->status = Token::ACCOUNT_CONFIRMATION;
//        $token->save();

        // enviar email de confirmação de conta


        return response()->json([$establishment->data], 201);
    }

    public function edit (EditEstablishmentRequest $request) {
        try {
            /** @var Establishment $establishment */
            $establishment = Establishment::find($request->id);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            $body = $request->all();

            // this data can't update
            unset($body['cnpj']);
            unset($body['email']);
            unset($body['senha']);
            unset($body['senha_confirmation']);

            $establishment->update($request->all());

            return response()->json(['establishment' => $establishment], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * todo - save and edit card credit data
     * @param Request $request
     */
    public function addCreditCard (Request $request) {
        //
    }

    /**
     * Cria e edita contato do estabelecimento
     * @param AddContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addContact (AddContactRequest $request) {

        try {

            /**
             * @var Establishment $establishment
             */
            $establishment = Establishment::with('contacts')
                ->where('_id', $request->id)
                ->first();

            if (empty($establishment)) return response()->json(["message" => "Estabelecimento não encontrado"], 404);

            $contact = $establishment->contacts;
            if (empty($contact)) {
                $contact = new Contact();
                $contact->status = Contact::STATUS_ON;
            }

            // altero os valores do contato
            $contact->celular = $request->celular;
            $contact->whatsapp = $request->whatsapp;
            $contact->minimo_horario_receber_chamada = $request->minimo_horario_receber_chamada;
            $contact->maximo_horario_receber_chamada = $request->maximo_horario_receber_chamada;

            // salva contado do estabelecimento
            $establishment->contacts()->save($contact);

            return response()->json(['establishment' => $establishment->refresh()], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * save and edit address data
     * @param AddAddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAddress (AddAddressRequest $request) {
        /** @var Establishment $establishment */
        $establishment = Establishment::find($request->id);

        if (empty($establishment)) return response()->json(["message" => "Estabelecimento não encontrado"], 404);

        try {
            $addressEntitie = new AddressEntitie();
            $addressEntitie->setAttributesWithRequest($request);

            $body = $addressEntitie->getAllAttributes();

            $address = new Address($body);
            $address->status = Address::STATUS_ON;
            $address->updated_at = date('Y-m-d H:i:s', time());

            if (!empty($establishment->addresses)) $establishment->addresses()->update($address->getAttributes());
            else $establishment->addresses()->save($address);

            return response()->json($establishment->refresh(), 201);

        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * save and edit client
     * @param AddClientRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addClient (AddClientRequest $request) {

        try {
            /** @var Establishment $establishment */
            $establishment = Establishment::find($request->id);
            $establishment->load('clients');

            if (empty($establishment)) return response()->json(["message" => "Estabelecimento não encontrado"], 404);

            // adiciona cliente
            $client = new ClientEntitie();
            $client->setAttributesWithRequest($request);

            // adiciona endereço
            if (!empty($request->cep)) {
                $address = new \App\Entities\Address();
                $address->setAttributesWithRequest($request);
                $client->setAddress($address);
            }

            // adiciona contato
            if (!empty($request->celular)) {
                $contact = new \App\Entities\Contact();
                $contact->setAttributesWithRequest($request);
                $client->setContact($contact);
            }

            // remove cliente existente e cadastra novamente com os novos dados informados
            if (!empty($request->clientId)) {
                $clientRemove = Client::find($request->clientId);
                if (empty($clientRemove)) throw new \Exception("Este cliente não existe mais ou foi atualizado", 400);
                $clientRemove->delete();
            }

            // salva novo cliente no estabelecimento
            $lastSave = $establishment->clients()->save(new Client($client->getAllAttributes()));

            return response()->json(["clients" => $establishment->refresh(), "lastClientSaved" => $lastSave], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

    }

    // orders
    public function removeDeliveryman (Request $request) {
        try {
            $establishment = Establishment::find($request->id);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            // verifica se a entrega pertence ao estabelecimento
            if ($establishment->_id !== $order->establishmentId) throw new \Exception("Este pedido não pertence ao estabelecimento", 400);

            // verifica se existe entregador na entrega
            if (empty($order->deliveryManId)) throw new \Exception("Não tem nenhum entregador responsável nesta entrega", 200);

            // verifica status antes de cancelar
            $statusNotAllowedCancel = [Order::STATUS_WAITING_PAYMENT, Order::STATUS_CANCELED, Order::STATUS_ORDER_DELIVERED];
            if (in_array($order->status, $statusNotAllowedCancel)) throw new \Exception("Não é possivel executar esta ação com a entrega no status atual.", 400);

            $order->deliveryManId = '';
            $order->status = Order::STATUS_PENDING;

            // todo - enviar notificação para o entregador avisando que o estabeleciento cancelou a entrega

            $order->save();

            return response()->json(['message' => 'O entregador foi retirado e o pedido voltou para status pendente.'], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * [confirmOutput description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function confirmOutput (Request $request) {

        try {
            // validar se o estabelecimento existe
            $establishment = Establishment::find($request->id);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            // validar se o pedido existe
            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            // validar se o estabelecimento é o mesmo estabelecimento que está salvo no registro do pedido
            if ($establishment->_id !== $order->establishmentId) throw new \Exception("Este pedido não pertence a este estabelecimento", 400);

            // verifica se tem entregador no pedido
            if (empty($order->deliveryManId)) throw new \Exception("Aguarde um entregador aceitar a entrega", 400);

            if ($order->status == Order::STATUS_ORDER_DELIVERED) throw new \Exception("Entrega já foi realizada.", 200);
            if ($order->status !== Order::STATUS_DELIVERYMAN_ACCEPT) throw new \Exception("Nenhum entregador aceitou a entrega ou a entrega já foi liberada", 400);

            // trocar status do pedido para "estabelecimento liberou a entrega"
            $order->status = Order::STATUS_DELIVERYMAN_WAY;
            $order->save();

            return response()->json(['message' => 'O pedido foi liberado para saída'], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function orderDone (Request $request) {
        try {
            $establishment = Establishment::find($request->id);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            // verifica se a entrega pertence ao estabelecimento
            if ($establishment->_id !== $order->establishmentId) throw new \Exception("Esta entrega não pertence à seu estabelecimento", 400);

            // verifica se o entregador confirmou a entrega
            if ($order->status == Order::STATUS_ORDER_DELIVERED) throw new \Exception("Entrega já foi realizada.", 200);
            if ($order->status !== Order::STATUS_DELIVERYMAN_CONFIRMED) throw new \Exception("O entregador ainda não confirmou a entrega", 400);

            $order->status = Order::STATUS_ORDER_DELIVERED;
            $order->paymentDelivery = Order::PAYMENT_DELIVERYMAN_NO;

            $order->save();

            return response()->json(['message' => 'Entrega finalizada com sucesso!'], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function noDelivered (Request $request) {
        try {
            $establishment = Establishment::find($request->id);
            if (empty($establishment)) throw new \Exception("Estabelecimento não encontrado", 404);

            $order = Order::find($request->orderId);
            if (empty($order)) throw new \Exception("Pedido não encontrado", 404);

            // verifica se a entrega pertence ao estabelecimento
            if ($establishment->_id !== $order->establishmentId) throw new \Exception("Esta entrega não pertence à seu estabelecimento", 400);

            // verifica se o entregador confirmou a entrega
            if ($order->status == Order::STATUS_ORDER_DELIVERED) throw new \Exception("Entrega já foi realizada.", 200);
            if ($order->status !== Order::STATUS_DELIVERYMAN_CONFIRMED) throw new \Exception("O entregador ainda não confirmou a entrega", 400);

            $order->status = Order::STATUS_DELIVERYMAN_WAY;

            $order->save();

            return response()->json(['message' => 'Entrega voltou para o status de à caminho'], 200);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
