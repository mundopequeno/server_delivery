<?php

namespace App\Http\Middleware;

use App\Establishment;
use App\Token;
use Closure;

class APIAuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * name kernel ===> apiauthentication
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            // return response()->json($request->header('apitoken'));

            // verifica se o header foi informado
            if (empty($request->header('apitoken'))) throw new \Exception("Token de acesso não informado", 401);

            /** @var \App\Token $token */
            $token = Token::with('establishment')
                ->where('token', $request->header('apitoken'))
                ->first();

            // valida token
            if (empty($token)) throw new \Exception("Token informádo é inválido", 401);
            if (!$token->isValid()) throw new \Exception("Este token não está mais válido", 401);

            // valida estabelecimento que tem o token
            $establishment = $token->establishment;
            if (empty($establishment)) throw new \Exception("Nenhum estabelecimento encontrado", 401);
            if ($establishment->status = Establishment::STATUS_OFF) throw new \Exception("Estabelecimento desativado temporariamente", 401);

            return $next($request);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
