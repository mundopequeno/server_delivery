<?php

namespace App\Http\Middleware;

use App\DeliveryMan;
use App\Token;
use Closure;

class APIAuthenticationDeliverymanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            // verifica se o header foi informado
            if (empty($request->header('apitoken'))) throw new \Exception("Token de acesso não informado", 401);

            /** @var \App\Token $token */
            $token = Token::with('deliveryman')
                ->where('token', $request->header('apitoken'))
                ->first();

            // valida token
            if (empty($token)) throw new \Exception("Token informádo está inválido", 401);
            if (!$token->isValid()) throw new \Exception("Este token não está mais válido", 401);

            // valida estabelecimento que tem o token
            $deliverymanModel = $token->deliveryman;
            if (empty($deliverymanModel)) throw new \Exception("Nenhum entregador encontrado", 401);
            if ($deliverymanModel->status = DeliveryMan::STATUS_OFF) throw new \Exception("Sua conta está desativada temporariamente", 401);

            return $next($request);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
