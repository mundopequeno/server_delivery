<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 14/03/18
 * Time: 03:15
 */

namespace App\Entities;


use App\Constants\VehicleTypes;
use App\Interfaces\BuildModelToEntitie;
use App\Interfaces\BuildRequestToEntitie;
use App\Interfaces\EntitieGenerateJSON;
use Illuminate\Http\Request;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Vehicle implements EntitieGenerateJSON, BuildRequestToEntitie, BuildModelToEntitie
{
    private $placa;
    private $renavam;
    private $cor;
    private $modelo;
    private $tipo;

    /**
     * @return mixed
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * @param mixed $placa
     * @throws \Exception
     */
    public function setPlaca($placa)
    {
        if (empty($placa)) throw new \Exception("Informe a placa do veículo", 400);
        $this->placa = trim($placa);
    }

    /**
     * @return mixed
     */
    public function getRenavam()
    {
        return $this->renavam;
    }

    /**
     * @param mixed $renavam
     */
    public function setRenavam($renavam)
    {
        $this->renavam = $renavam;
    }

    /**
     * @return mixed
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * @param mixed $cor
     * @throws \Exception
     */
    public function setCor($cor)
    {
        if (empty($cor)) throw new \Exception("Informe a cor do veículo", 400);
        $this->cor = trim($cor);
    }

    /**
     * @return mixed
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @param mixed $modelo
     * @throws \Exception
     */
    public function setModelo($modelo)
    {
        if (empty($modelo)) throw new \Exception("Informe o modelo do veículo", 400);
        $this->modelo = $modelo;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     * @throws \Exception
     */
    public function setTipo($tipo)
    {
        if (!key_exists($tipo, VehicleTypes::TYPES)) throw new \Exception("Veiculo não permitido", 400);
        $this->tipo = VehicleTypes::TYPES[$tipo]['nome'];
    }



    /**
     * Este método é para retornar todos os atributos da classe
     * @return array
     */
    public function getAllAttributes(): array
    {
        return [
            'placa' => $this->getPlaca(),
            'renavam' => $this->getRenavam(),
            'cor' => $this->getCor(),
            'modelo' => $this->getModelo(),
            'tipo' => $this->getTipo()
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function setAttributesWithRequest(Request $request): void
    {
        $this->setPlaca($request->placa);
        $this->setRenavam($request->renavam);
        $this->setCor($request->cor);
        $this->setModelo($request->modelo);
        $this->setTipo($request->tipo);
    }

    /**
     * Monta a entidade a partir do modelo
     * @param Moloquent $model
     * @return bool
     * @throws \Exception
     */
    public function setAttributesWithModel(Moloquent $model): bool
    {
        $vehicle = $model->vehicle;
        if (empty($vehicle)) return false;

        $this->setPlaca($vehicle['placa']);
        $this->setRenavam($vehicle['renavam']);
        $this->setCor($vehicle['cor']);
        $this->setModelo($vehicle['modelo']);
        $this->setTipo($vehicle['tipo']);

        return true;
    }
}