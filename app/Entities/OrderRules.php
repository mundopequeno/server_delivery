<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 01/03/2018
 * Time: 18:44
 */

namespace App\Entities;


use App\Constants\VehicleTypes;

class OrderRules
{
    const PRICE_BY_DISTANCE = 1.5;
    const PRICE_BY_GROUP_ITEMS = 0.20;
    const PLATAFORM_PERCENTAGE = 0.10;
    const MINIMUM_PRICE = 1.50;
    const VEHICLE_RULES = [
        VehicleTypes::TYPES['pessoa']['nome'] => VehicleTypes::TYPES['pessoa']['taxa'],
        VehicleTypes::TYPES['moto']['nome'] => VehicleTypes::TYPES['moto']['taxa'],
        VehicleTypes::TYPES['carro']['nome'] => VehicleTypes::TYPES['carro']['taxa'],
        VehicleTypes::TYPES['caminhao']['nome'] => VehicleTypes::TYPES['caminhao']['taxa'],
    ];

    /**
     * Calcula o valor minimo da entrega
     * @param string $keyVehicleRule
     * @return double
     */
    public static function getMinimumPrice ($keyVehicleRule) {
        return self::VEHICLE_RULES[$keyVehicleRule] * self::MINIMUM_PRICE;
    }

    /**
     * Calcula o valor da distancia + jurus por kilometro
     * @param int $distance
     * @return float|int
     */
    public static function getDistancePrice ($distance) {
        return (($distance * self::PRICE_BY_DISTANCE) /1000) + ($distance / 1000);
    }

    /**
     * Calcula adicional por items
     * @param int $quantity
     * @return double
     */
    public static function getPriceByItems ($quantity) {
        return $quantity * self::PRICE_BY_GROUP_ITEMS;
    }
}