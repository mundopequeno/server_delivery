<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/03/18
 * Time: 19:25
 */

namespace App\Entities;


use App\DeliveryMan;
use App\Establishment;

class Token
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var Establishment|DeliveryMan
     */
    public $model;

    /**
     * Token constructor.
     * @param DeliveryMan|Establishment $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }


}