<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 09/03/18
 * Time: 20:43
 */

namespace App\Entities;


use App\Interfaces\BuildModelToEntitie;
use App\Interfaces\BuildRequestToEntitie;
use App\Interfaces\EntitieGenerateJSON;
use Illuminate\Http\Request;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Contact implements EntitieGenerateJSON, BuildRequestToEntitie, BuildModelToEntitie
{
    private $celular;
    private $whatsapp;
    private $minimo_horario_receber_chamada;
    private $maximo_horario_receber_chamada;

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return empty($this->celular) ? "" : $this->celular;
    }

    /**
     * @param mixed $celular
     * @throws \Exception
     */
    public function setCelular($celular)
    {
        if (empty($celular)) throw new \Exception('Celular não foi informado', 400);

        preg_match('/^\\(\\d{2}\\)\\d{4,5}\\-\\d{4}$/', $celular, $matchCelular);
        if (empty($matchCelular)) throw new \Exception("Formato de celular inválido", 400);
        $this->celular = $celular;
    }

    /**
     * @return int
     */
    public function getWhatsapp(): int
    {
        return empty($this->whatsapp) ? 0 : 1;
    }

    /**
     * @param mixed $whatsapp
     */
    public function setWhatsapp(int $whatsapp)
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return mixed
     */
    public function getMinimoHorarioReceberChamada()
    {
        return empty($this->minimo_horario_receber_chamada) ? "00:00" : $this->minimo_horario_receber_chamada;
    }

    /**
     * @param mixed $minimo_horario_receber_chamada
     */
    public function setMinimoHorarioReceberChamada($minimo_horario_receber_chamada)
    {
        $this->minimo_horario_receber_chamada = $minimo_horario_receber_chamada;
    }

    /**
     * @return mixed
     */
    public function getMaximoHorarioReceberChamada()
    {
        return empty($this->maximo_horario_receber_chamada) ? "00:00" : $this->maximo_horario_receber_chamada;
    }

    /**
     * @param mixed $maximo_horario_receber_chamada
     */
    public function setMaximoHorarioReceberChamada($maximo_horario_receber_chamada)
    {
        $this->maximo_horario_receber_chamada = $maximo_horario_receber_chamada;
    }


    /**
     * Este método é para retornar todos os atributos da classe
     * @return array
     */
    public function getAllAttributes(): array
    {
        return [
            'celular' => $this->getCelular(),
            'whatsapp' => $this->getWhatsapp(),
            'minimo_horario_receber_chamada' => $this->getMinimoHorarioReceberChamada(),
            'maximo_horario_receber_chamada' => $this->getMaximoHorarioReceberChamada()
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function setAttributesWithRequest(Request $request): void
    {
        $this->setCelular($request->celular);
        $this->setWhatsapp((int) $request->whatsapp);
        $this->setMinimoHorarioReceberChamada($request->minimo_horario_receber_chamada);
        $this->setMaximoHorarioReceberChamada($request->maximo_horario_receber_chamada);
    }

    /**
     * Monta a entidade a partir do modelo
     * @param Moloquent $model
     * @return bool
     * @throws \Exception
     */
    public function setAttributesWithModel(Moloquent $model): bool
    {
        $contact = $model->contact;
        if (empty($contact) || empty($contact['celular'])) return false;

        $this->setCelular($contact['celular']);
        $this->setWhatsapp((int) $contact['whatsapp']);
        $this->setMinimoHorarioReceberChamada($contact['minimo_horario_receber_chamada']);
        $this->setMaximoHorarioReceberChamada($contact['maximo_horario_receber_chamada']);

        return true;
    }
}