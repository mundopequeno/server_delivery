<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 09/03/18
 * Time: 20:31
 */

namespace App\Entities;


use App\Entities\Address;
use App\Interfaces\BuildRequestToEntitie;
use App\Interfaces\EntitieGenerateJSON;
use Illuminate\Http\Request;

class Client implements EntitieGenerateJSON, BuildRequestToEntitie
{
    /** @var string $nome */
    private $nome;

    /** @var string $cpf */
    private $cpf;

    /** @var Address */
    private $address;

    /** @var Contact $contact */
    private $contact;

    public function __construct()
    {
        $this->nome = '';
        $this->cpf = '';
        $this->address = new Address();
        $this->contact = new \App\Entities\Contact();
    }


    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getCpf(): string
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     * @throws \Exception
     */
    public function setCpf(string $cpf)
    {
        preg_match('/^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$/', $cpf, $matchCpf);
        if (empty($matchCpf)) throw new \Exception("Formato do cpf esta inválido", 400);
        $this->cpf = $cpf;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }


    /**
     * Este método é para retornar todos os atributos da classe
     * @return array
     */
    public function getAllAttributes(): array
    {
        return [
            'nome' => $this->getNome(),
            'cpf' => $this->getCpf(),
            'address' => $this->getAddress()->getAllAttributes(),
            'contact' => $this->getContact()->getAllAttributes()
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function setAttributesWithRequest(Request $request): void
    {
        $this->setNome($request->nome);
        $this->setCpf($request->cpf);
    }
}