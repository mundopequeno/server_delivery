<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 01/03/2018
 * Time: 01:53
 */

namespace App\Entities;


use App\Interfaces\BuildModelToEntitie;
use App\Interfaces\BuildRequestToEntitie;
use App\Interfaces\EntitieGenerateJSON;
use App\Tools\ValidatorTools;
use Illuminate\Http\Request;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Address implements EntitieGenerateJSON, BuildRequestToEntitie, BuildModelToEntitie
{
    const TIPOS_PERMITIDOS = ['casa', 'apartamento', 'estabelecimento'];

    private $cep;
    private $number;
    private $street;
    private $neighborhood;
    private $city;
    private $state;
    private $complement;
    private $reference;
    private $latitude;
    private $longitude;
    private $type_residence;

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     * @throws \Exception
     */
    public function setCep($cep)
    {
        if (empty($cep)) throw new \Exception("O Cep deve ser informado", 400);
        else if (!ValidatorTools::Cep($cep)) throw new \Exception("Cep com formato incorreto {$cep}", 400);
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     * @throws \Exception
     */
    public function setNumber($number)
    {
        if (empty($number)) throw new \Exception("O número deve ser informado", 400);
        else if (!ValidatorTools::AddressNumber($number)) throw new \Exception("Número com formáto incorreto", 400);
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param mixed $neighborhood
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * @param mixed $complement
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getTypeResidence()
    {
        return $this->type_residence;
    }

    /**
     * @param mixed $type_residence
     * @throws \Exception
     */
    public function setTypeResidence($type_residence)
    {
        if (empty($type_residence)) return null;

        $type_residence = strtolower(trim($type_residence));

        if (!in_array($type_residence, self::TIPOS_PERMITIDOS)) throw new \Exception("Tipo de endereço não identificado", 400);

        $this->type_residence = $type_residence;
    }

    /**
     * Este método é para retornar todos os atributos da classe
     * @return array
     */
    public function getAllAttributes(): array
    {
        return [
            'cep' => $this->getCep(),
            'number' => $this->getNumber(),
            'street' => $this->getStreet(),
            'neighborhood' => $this->getNeighborhood(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'complement' => $this->getComplement(),
            'reference' => $this->getReference(),
            'latitude' => $this->getLatitude(),
            'longitude' => $this->getLongitude(),
            'type_residence' => $this->getTypeResidence(),
        ];
    }

    /**
     * @param Request $request
     * @throws \Exception
     * @return void
     */
    public function setAttributesWithRequest (Request $request): void {
        $this->setCep($request->cep);
        $this->setNumber($request->number);
        $this->setStreet($request->street);
        $this->setNeighborhood($request->neighborhood);
        $this->setCity($request->city);
        $this->setState($request->state);
        $this->setComplement($request->complement);
        $this->setReference($request->reference);
        $this->setLatitude($request->latitude);
        $this->setLongitude($request->longitude);
        $this->setTypeResidence($request->type_residence);
    }

    /**
     * @param Moloquent $model
     * @return bool
     * @throws \Exception
     */
    public function setAttributesWithModel (Moloquent $model): bool {
        $address = $model->address;
        if (empty($address)) return false;

        $this->setCep($address['cep']);
        $this->setNumber($address['number']);
        $this->setStreet($address['street']);
        $this->setNeighborhood($address['neighborhood']);
        $this->setCity($address['city']);
        $this->setState($address['state']);
        $this->setComplement($address['complement']);
        $this->setReference($address['reference']);
        $this->setLatitude($address['latitude']);
        $this->setLongitude($address['longitude']);
        $this->setTypeResidence($address['type_residence']);

        return true;
    }
}