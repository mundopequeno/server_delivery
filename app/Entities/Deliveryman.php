<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/03/18
 * Time: 00:23
 */

namespace App\Entities;


use App\Constants\ImagesPath;
use App\Interfaces\BuildModelToEntitie;
use App\Interfaces\BuildRequestToEntitie;
use App\Interfaces\EntitieGenerateJSON;
use App\Tools\ValidatorTools;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Deliveryman implements EntitieGenerateJSON, BuildRequestToEntitie, BuildModelToEntitie
{
    /** @var string */
    private $nome;

    /** @var string */
    private $sobrenome;

    /** @var string */
    private $rg;

    /** @var string */
    private $cpf;

    /** @var string */
    private $dataNascimento;

    /** @var string */
    private $email;

    /** @var string */
    private $foto;

    /** @var string */
    private $senha;

    /** @var Address */
    private $address;

    /** @var Contact */
    private $contact;

    /** @var Vehicle */
    private $vehicle;

    /** @var int */
    private $status;

    /** @var int */
    private $hasVehicle;

    /** @var int */
    private $hasContact;

    /** @var int */
    private $hasAddress;

    /**
     * Deliveryman constructor.
     * @param string $nome
     * @param string $sobrenome
     * @param string $rg
     * @param string $cpf
     * @param string $dataNascimento
     * @param string $email
     * @param string $foto
     * @param string $senha
     */
    public function __construct(string $nome = '', string $sobrenome = '', string $rg = '', string $cpf = '', string $dataNascimento = '', string $email = '', string $foto = '', string $senha = '')
    {
        $this->nome = $nome;
        $this->sobrenome = $sobrenome;
        $this->rg = $rg;
        $this->cpf = $cpf;
        $this->dataNascimento = $dataNascimento;
        $this->email = $email;
        $this->foto = $foto;
        $this->senha = $senha;
        $this->address = new Address();
        $this->contact = new Contact();
        $this->vehicle = new Vehicle();
    }


    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @throws \Exception
     */
    public function setNome(string $nome)
    {
        if (empty($nome)) throw new \Exception("Nome não foi informado", 400);
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getSobrenome(): string
    {
        return $this->sobrenome;
    }

    /**
     * @param string $sobrenome
     * @throws \Exception
     */
    public function setSobrenome(string $sobrenome)
    {
        if (empty($sobrenome)) throw new \Exception("Sobrenome não foi informado", 400);
        $this->sobrenome = $sobrenome;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getRg(): string
    {
        if (empty($this->rg)) throw new \Exception("Por favor, informe seu RG", 400);
        return $this->rg;
    }

    /**
     * @param string $rg
     * @throws \Exception
     */
    public function setRg($rg)
    {
        if (empty($rg)) throw new \Exception("RG não foi informado", 400);

        if (!ValidatorTools::Rg($rg)) throw new \Exception("RG está com formato inválido", 400);
        $this->rg = $rg;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getCpf(): string
    {
        if (empty($this->cpf)) throw new \Exception("Por favor, informe seu CPF", 400);
        return $this->cpf;
    }

    /**
     * @param string $cpf
     * @throws \Exception
     */
    public function setCpf($cpf)
    {
        if (empty($cpf)) throw new \Exception("Cep não foi informado", 400);

        if (!ValidatorTools::Cpf($cpf)) throw new \Exception("Cep está com formato inválido", 400);
        $this->cpf = $cpf;
    }

    /**
     * @return string
     */
    public function getDataNascimento(): string
    {
        return $this->dataNascimento;
    }

    /**
     * @param string $dataNascimento
     * @throws \Exception
     */
    public function setDataNascimento(string $dataNascimento)
    {
        if (empty($dataNascimento)) throw new \Exception("Data de nascimento não foi informado", 400);
        $this->dataNascimento = $dataNascimento;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @throws \Exception
     */
    public function setEmail(string $email)
    {
        if (empty($email)) throw new \Exception("E-mail não foi informado", 400);

        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFoto(): string
    {
        return $this->foto;
    }

    /**
     * @param UploadedFile|null $foto
     */
    public function setFoto($foto)
    {
        if (!empty($foto)) {
            $this->foto = $foto->store(ImagesPath::DELIVERYMAN_PICTURE, 'public');
        }
    }

    /**
     * @return string
     */
    public function getSenha(): string
    {
        return $this->senha;
    }

    /**
     * @param string $senha
     * @throws \Exception
     */
    public function setSenha(string $senha)
    {
        if (empty($senha)) throw new \Exception("Senha não foi informado", 400);

        $this->senha = encrypt($senha);
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return Vehicle
     */
    public function getVehicle(): Vehicle
    {
        return $this->vehicle;
    }

    /**
     * @param Vehicle $vehicle
     */
    public function setVehicle(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status | 0;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getHasVehicle(): int
    {
        return $this->hasVehicle | 0;
    }

    /**
     * @param int $hasVehicle
     */
    public function setHasVehicle(int $hasVehicle)
    {
        $this->hasVehicle = $hasVehicle;
    }

    /**
     * @return int
     */
    public function getHasContact(): int
    {
        return $this->hasContact | 0;
    }

    /**
     * @param int $hasContact
     */
    public function setHasContact(int $hasContact)
    {
        $this->hasContact = $hasContact;
    }

    /**
     * @return int
     */
    public function getHasAddress(): int
    {
        return $this->hasAddress | 0;
    }

    /**
     * @param int $hasAddress
     */
    public function setHasAddress(int $hasAddress)
    {
        $this->hasAddress = $hasAddress;
    }


    /**
     * Este método é para retornar todos os atributos da classe
     * @return array
     * @throws \Exception
     */
    public function getAllAttributes(): array
    {
        return [
            'nome' => $this->getNome(),
            'sobrenome' => $this->getSobrenome(),
            'rg' => $this->getRg(),
            'cpf' => $this->getCpf(),
            'dataNascimento' => $this->getDataNascimento(),
            'email' => $this->getEmail(),
            'senha' => $this->getSenha(),
            'foto' => $this->getFoto(),
            'status' => $this->getStatus(),
            'hasAddress' => $this->getHasAddress(),
            'hasContact' => $this->getHasContact(),
            'hasVehicle' => $this->getHasVehicle(),
            'address' => $this->getAddress()->getAllAttributes(),
            'contact' => $this->getContact()->getAllAttributes(),
            'vehicle' => $this->getVehicle()->getAllAttributes()
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function setAttributesWithRequest(Request $request): void
    {
        $this->setNome($request->nome);
        $this->setSobrenome($request->sobrenome);
//        $this->setRg($request->rg);
//        $this->setCpf($request->cpf);
        $this->setDataNascimento($request->dataNascimento);
        $this->setEmail($request->email);
        $this->setSenha($request->senha);
        $this->setFoto($request->file('foto'));
    }

    /**
     * @param Moloquent $deliveryMan
     * @return bool
     * @throws \Exception
     */
    public function setAttributesWithModel (Moloquent $deliveryMan): bool {
        $this->setNome($deliveryMan->nome);
        $this->setSobrenome($deliveryMan->sobrenome);
        $this->setRg($deliveryMan->rg);
        $this->setCpf($deliveryMan->cpf);
        $this->setDataNascimento($deliveryMan->dataNascimento);
        $this->setEmail($deliveryMan->email);
        $this->setSenha(decrypt($deliveryMan->senha));
        $this->setFoto($deliveryMan->foto);
        $this->setHasContact($deliveryMan->hasContact);
        $this->setHasAddress($deliveryMan->hasAddress);
        $this->setHasVehicle($deliveryMan->hasVehicle);

        // set address
        $addressEntitie = new Address();
        $addressEntitie->setAttributesWithModel($deliveryMan);
        $this->setAddress($addressEntitie);

        // set contact
        $contactEntitie = new Contact();
        $contactEntitie->setAttributesWithModel($deliveryMan);
        $this->setContact($contactEntitie);

        // set vehicle
        $vehicleEntitie = new Vehicle();
        $vehicleEntitie->setAttributesWithModel($deliveryMan);
        $this->setVehicle($vehicleEntitie);

        return true;
    }
}