<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 01/03/2018
 * Time: 00:57
 */

namespace App\Entities;


use App\Interfaces\BuildRequestToEntitie;
use App\Interfaces\EntitieGenerateJSON;
use Illuminate\Http\Request;

class GroupItems implements EntitieGenerateJSON, BuildRequestToEntitie
{
    const TIPOS_PERMITIDOS = ['sacola', 'caixa'];

    private $tipo;

    /**
     * @var Item[]
     */
    private $listItems;

    /**
     * GroupItems constructor.
     * @param string $tipo
     * @param Item[] $listItems
     * @throws \Exception
     */
    public function __construct($tipo, array $listItems = [])
    {
        $this->setTipo($tipo);
        $this->setArrayListItems($listItems);
    }

    /**
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     * @throws \Exception
     */
    public function setTipo($tipo)
    {
        if (!isset($tipo) && empty($tipo)) throw new \Exception("O tipo de grupo dos items não foi informado", 400);

        $tipo = strtolower(trim($tipo));

        if (!in_array($tipo, self::TIPOS_PERMITIDOS)) throw new \Exception("Tipo de grupo não permitido", 400);

        $this->tipo = $tipo;
    }

    /**
     * @return Item[]
     */
    public function getListItems()
    {
        return $this->listItems;
    }

    /**
     * @return string[]
     */
    public function getListItemsArray()
    {
        $listItems = [];
        /** @var Item $item */
        foreach ($this->listItems as $item) $listItems[] = $item->getAllAttributes();

        return $listItems;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->listItems[] = $item;
    }

    /**
     * @param Item[] $listItems
     */
    public function setArrayListItems($listItems)
    {
        foreach ($listItems as $item) $this->listItems[] = $item;
    }

    /**
     * @return array
     */
    public function getAllAttributes(): array
    {
        return [
            'tipo' => $this->getTipo(),
            'listItems' => $this->getListItemsArray()
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function setAttributesWithRequest(Request $request): void
    {
        throw new \Exception("Não programatico este metodo ainda");
    }
}