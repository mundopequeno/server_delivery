<?php 

namespace App\Entities;


use App\Interfaces\EntitieGenerateJSON;
use Illuminate\Http\Request;

class Item implements EntitieGenerateJSON
{
	private $nome;
	private $quantidade;

    /**
     * Item constructor.
     * @param $nome
     * @param $quantidade
     * @throws \Exception
     */
    function __construct($nome, $quantidade)
	{
		$this->setNome($nome);
		$this->setQuantidade($quantidade);
	}

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @throws \Exception
     */
    public function setNome($nome)
    {
        if (empty($nome)) throw new \Exception("Nome deve ser informado", 400);
        $this->nome = ucfirst(trim($nome));
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     * @throws \Exception
     */
    public function setQuantidade($quantidade)
    {
        if (empty($quantidade)) throw new \Exception("Quantidade do item adicionado deve ser informado", 400);
        else if ($quantidade > 100) throw new \Exception("Quantidade máxima excedida", 400);

        $this->quantidade = $quantidade;
    }

    /**
     * @return array
     */
    public function getAllAttributes(): array
    {
        return [
            'nome' => $this->getNome(),
            'quantidade' => $this->getQuantidade()
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function setAttributesWithRequest(Request $request): void
    {
        $this->setNome($request->nome);
        $this->setQuantidade($request->quantidade);
    }
}