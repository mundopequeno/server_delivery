<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Establishment extends Moloquent
{
    const TABLE_NAME = "establishments";

    const STATUS_ON = 1;
    const STATUS_OFF = 0;
    const STATUS_PAYMENT = 2;
    const STATUS_MISSING_DATA = 3;

    protected $collection = self::TABLE_NAME;
    protected $hidden = ['senha'];

    protected $fillable = ['nome', 'cnpj', 'email', 'senha', 'tipo', 'horario_aberto', 'horario_fechado', 'status'];


    public function token () {
        return $this->hasOne('\App\Token', 'ownerId');
    }

    public function addresses () {
        return $this->hasOne('\App\Address', 'belongsToId');
    }

    public function contacts () {
        return $this->hasOne('\App\Contact', 'belongsToId');
    }

    public function clients () {
        return $this->hasMany('\App\Client', 'belongsToId');
    }

    public function orders () {
        return $this->hasMany('\App\Order', 'establishmentId');
    }

    /**
     * @param string $nome
     * @param string $cnpj
     * @param string $email
     * @param string $senha
     * @param string $tipo
     * @param string $horario_aberto
     * @param string $horario_fechado
     * @param string $status
     */
    public function build ($nome, $cnpj, $email, $senha, $tipo, $horario_aberto, $horario_fechado, $status) {
        $this->nome = $nome;
        $this->cnpj = $cnpj;
        $this->email = $email;
        $this->senha = encrypt($senha);
        $this->tipo = $tipo;
        $this->horario_aberto = $horario_aberto;
        $this->horario_fechado = $horario_fechado;
        $this->status = $status;
    }
}
