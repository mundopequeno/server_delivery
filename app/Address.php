<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Address extends Moloquent
{
    const TABLE_NAME = "addresses";
    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    const RESIDENCE_HOME = 'casa';
    const RESIDENCE_ESTABLISHMENT = 'estabelecimento';
    const RESIDENCE_APARTMENT = 'apartamento';
    const RESIDENCE_CONDOMINIO_HOME = 'condominio_casa';
    const RESIDENCE_CONDOMINIO_APARTMENT = 'condominio_apartamento';

    const ALLOW_RESIDENCES = [
        self::RESIDENCE_HOME,
        self::RESIDENCE_ESTABLISHMENT,
        self::RESIDENCE_CONDOMINIO_HOME,
        self::RESIDENCE_APARTMENT,
        self::RESIDENCE_CONDOMINIO_APARTMENT
    ];

    protected $collection = self::TABLE_NAME;

    protected $fillable = [
        'belongsToId',
        'cep',
        'number',
        'street',
        'neighborhood',
        'city',
        'state',
        'complement',
        'reference',
        'latitude',
        'longitude',
        'type_residence',
        'status'
    ];

    /**
     * @param string $belongsToId
     * @param string $cep
     * @param int $number
     * @param string $street
     * @param string $neighborhood
     * @param string $city
     * @param string $state
     * @param string $complement
     * @param string $reference
     * @param string $latitude
     * @param string $longitude
     * @param int $type_residence
     * @param int $status
     */
    public function build ($belongsToId, $cep, $number, $street, $neighborhood, $city, $state, $complement, $reference, $latitude, $longitude, $type_residence, $status) {
        $this->belongsToId = $belongsToId;
        $this->cep = $cep;
        $this->number = $number;
        $this->street = $street;
        $this->neighborhood = $neighborhood;
        $this->city = $city;
        $this->state = $state;
        $this->complement = $complement;
        $this->reference = $reference;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->type_residence = $type_residence;
        $this->status = $status;
    }
}
