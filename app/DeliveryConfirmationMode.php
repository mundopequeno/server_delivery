<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class DeliveryConfirmationMode extends Moloquent
{
    const TABLE_NAME = "delivery_confirmation_modes";

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    protected $collection = self::TABLE_NAME;


    public function build ($name, $nameValidate, $description, $status) {
        $this->name = $name;
        $this->nameValidate = $nameValidate;
        $this->description = $description;
        $this->status = $status;
    }
}
