<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 14/03/18
 * Time: 04:21
 */

namespace App\Constants;


class VehicleTypes
{
    const TYPES = [
        'pessoa' => [
            'nome' => 'pessoa',
            'taxa' => 1.00
        ],
        'moto' => [
            'nome' => 'moto',
            'taxa' => 2.00
        ],
        'carro' => [
            'nome' => 'carro',
            'taxa' => 3.00
        ],
        'caminhao' => [
            'nome' => 'caminhao',
            'taxa' => 4.00
        ]
    ];
}