<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Client extends Moloquent
{
    const TABLE_NAME = "clients";

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    protected $collection = self::TABLE_NAME;


    protected $fillable = [
        'nome', 'cpf', 'address', 'contact'
    ];
}
