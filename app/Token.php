<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/03/18
 * Time: 19:36
 */

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Token extends Moloquent
{
    const TABLE_NAME = 'tokens';

    protected $collection = self::TABLE_NAME;

    const ACCOUNT_CONFIRMATION = 1;
    const AUTHENTICATION = 2;

    const TOKEN_DURATION = '+8 hours';

    protected $fillable = [
        'token', 'ownerId', 'status'
    ];

    public $timestamps = true;


    public function establishment () {
        return $this->belongsTo('\App\Establishment', 'ownerId');
    }

    public function deliveryman () {
        return $this->belongsTo('\App\DeliveryMan', 'ownerId');
    }

    /**
     * Referencia é usado para ajudar a gerar um token único. Este metodo já atualiza updated_at e token
     * @param Establishment|DeliveryMan $reference
     * @return string retorna token gerado
     */
    public function generate ($reference) {
        $new_token = encrypt($reference->_id . date('Y-m-d H:i:s'));

        $this->updated_at = date('Y-m-d H:i:s', time());
        $this->token = $new_token;

        return $new_token;
    }


    /**
     * verifica se o token é válido
     * @return bool
     */
    public function isValid () {
        $time_valid = new \DateTime($this->updated_at);
        $time_valid->modify(Token::TOKEN_DURATION);

        return $time_valid->getTimestamp() > time();
    }
}