<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

/**
 * Class Item
 * @package App
 * @property string $nome
 * @property string $descricao
 * @property string $peso
 * @property string $largura
 * @property string $altura
 * @property string $quantidade
 * @property int $status
 */
class Item extends Moloquent
{
    const TABLE_NAME = "items";

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    protected $collection = self::TABLE_NAME;


    /**
     * @param string $nome
     * @param string $descricao
     * @param string $peso
     * @param string $largura
     * @param string $altura
     * @param string $quantidade
     * @param int $status
     */
    public function build ($nome, $quantidade, $status, $descricao = null, $peso = null, $largura = null, $altura = null) {
        $this->nome = $nome;
        $this->quantidade = $quantidade;
        $this->descricao = $descricao;
        $this->peso = $peso;
        $this->largura = $largura;
        $this->altura = $altura;
        $this->status = $status;
    }
}
