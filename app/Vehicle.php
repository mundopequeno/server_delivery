<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Vehicle extends Moloquent
{
    const TABLE_NAME = "vehicles";

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    const TYPE_ALLOW = [
        "moto", "carro", "van", "caminhao"
    ];

    protected $collection = self::TABLE_NAME;

    public function allowVehicle () {
        return $this->belongsTo('\App\VehicleAllowed', 'allowVehicleId');
    }


    public function build ($belongsToId, $veiculo, $cnh, $placa, $cor, $ano, $allowVehicleId, $status) {
        $this->belongsToId = $belongsToId;
        $this->veiculo = $veiculo;
        $this->cnh = $cnh;
        $this->placa = $placa;
        $this->cor = $cor;
        $this->ano = $ano;
        $this->allowVehicleId = $allowVehicleId;
        $this->status = $status;
    }


    public function isPermitted () {
        return $this->allowVehicle && $this->status == self::STATUS_ON ? true : false;
    }
}
