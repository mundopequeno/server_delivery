<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;
use Mockery\Exception;

class Order extends Moloquent
{
    const TABLE_NAME = "orders";

    const STATUS_WAITING_PAYMENT = "aguardando_pagamento";
    const STATUS_PENDING = "pendente";
    const STATUS_CANCELED = "cancelado";
    const STATUS_DELIVERYMAN_ACCEPT = "entregador_aceitou";
    const STATUS_DELIVERYMAN_WAY = "entregador_a_caminho";
    const STATUS_DELIVERYMAN_CONFIRMED = "entregador_confirmou_entrega";
    const STATUS_ORDER_DELIVERED = "pedido_entregue";

    const PAYMENT_DELIVERYMAN_YES = "pagamento_entregador_realizado";
    const PAYMENT_DELIVERYMAN_NO = "pagamento_entregador_nao_realizado";

    const FIELD_ESTABLISHMENT_ID = 'establishmentId';

    protected $collection = self::TABLE_NAME;
    protected $fillable = [
        'establishmentId',
        'clientId',
        'shippingAddress',
        'deliveryManId',
        'items',
        'price',
        'status'
    ];


    public function establishment () {
        return $this->belongsTo('\App\Establishment', 'establishmentId')->with(['addresses', 'contacts']);
    }

    public function client () {
        return $this->belongsTo('\App\Client', 'clientId')->with(['addresses', 'contacts']);
    }

    public function allowVehicle () {
        return $this->belongsTo('\App\VehicleAllowed', 'allowVehicleId');
    }

    public function modeConfirmation () {
        return $this->belongsTo('\App\DeliveryConfirmationMode', 'modeConfirmationId');
    }


    public function build ($establishmentId, $clientId, $deliveryManId, $items, $deliveryPrice, $distanceToDestine, $allowVehicle, $modoConfirmation, $status) {
        $this->establishmentId = $establishmentId;
        $this->clientId = $clientId;
        $this->deliveryManId = $deliveryManId;
        $this->items = $items;
        $this->deliveryPrice = $deliveryPrice;
        $this->distanceToDestine = $distanceToDestine;
        $this->allowVehicleId = $allowVehicle;
        $this->modeConfirmationId = $modoConfirmation;
        $this->status = $status;
    }

    public function validateEstablishment (Establishment $establishment) {
        if ($this->establishmentId !== $establishment->_id) throw new \Exception("Esta entrega não pertence a seu estabelecimento", 400);
        return true;
    }
}
