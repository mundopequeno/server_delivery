<?php 

namespace App\Tools;

use App\Address;

class ValidatorTools 
{
	const REGEX_CEP = '/\\d{5}\\-\\d{3}/';
    const REGEX_RG = '/^\\d{2}\\.\\d{3}\\.\\d{3}\\-\\d+/';
    const REGEX_CPF = '/^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$/';
	const REGEX_ADDRESS_NUMBER = '/^\\d{1,4}$/';

	public static function Cep ($cep) {
		return !empty(preg_match(self::REGEX_CEP, $cep, $match));
	}

    public static function Cpf ($cep) {
        return !empty(preg_match(self::REGEX_CPF, $cep, $match));
    }

    public static function Rg ($rg) {
        return !empty(preg_match(self::REGEX_RG, $rg, $match));
    }

	public static function AddressNumber ($number) {
		return !empty(preg_match(self::REGEX_ADDRESS_NUMBER, $number, $match));
	}

	public static function AddressType ($type) {
		return in_array($type, Address::ALLOW_RESIDENCES);
	}
}