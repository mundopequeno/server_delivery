<?php

namespace App;

use App\Entities\Token;
use App\Interfaces\TokenService;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;


class DeliveryMan extends Moloquent
{
    const TABLE_NAME = "deliveryman";

    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    const STATUS_VALIDATE_EMAIL = 2;
    const STATUS_WAIT_VEICLE = 3;

    const HAS_VEHICLE = 1;
    const HAS_CONTACT = 1;
    const HAS_ADDRESS = 1;

    const HAS_NOT_VEHICLE = 0;
    const HAS_NOT_CONTACT = 0;
    const HAS_NOT_ADDRESS = 0;


    protected $collection = self::TABLE_NAME;

    protected $fillable = [
        'nome', 'sobrenome', 'rg', 'cpf', 'dataNascimento', 'email', 'foto', 'senha', 'address', 'contact', 'vehicle', 'status', 'hasVehicle', 'hasContact', 'hasAddress'
    ];

    public function addresses ()
    {
        return $this->hasMany('App\Address', 'belongsToId');
    }

    public function contacts ()
    {
        return $this->hasMany('App\Contact', 'belongsToId');
    }

//    public function vehicle () {
//        return $this->hasOne('App\Vehicle', 'belongsToId')->with(['allowVehicle']);
//    }

    public function token () {
        return $this->hasOne(\App\Token::class, 'referenceId');
    }


    /**
     * @throws \Exception
     */
    public function buildEntitie () {
        $deliverymanEntitie = new \App\Entities\Deliveryman();
        $deliverymanEntitie->setAttributesWithModel($this);
    }


    /**
     * DeliveryMan constructor.
     * @param String $nome
     * @param String $sobrenome
     * @param String $rg
     * @param String $cpf
     * @param String $dataNascimento
     * @param String $email
     * @param String $foto
     * @param String $senha
     */
    public function build ($nome, $sobrenome, $rg, $cpf, $dataNascimento, $email, $foto, $senha, $status)
    {
        $this->name = $nome;
        $this->last_name = $sobrenome;
        $this->rg = $rg;
        $this->cpf = $cpf;
        $this->dateOfBirth = $dataNascimento;
        $this->email = $email;
        $this->picture = $foto;
        $this->password = $senha;
    }
}
