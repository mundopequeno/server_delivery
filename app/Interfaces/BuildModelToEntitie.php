<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 14/03/18
 * Time: 04:56
 */

namespace App\Interfaces;


use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

interface BuildModelToEntitie
{
    /**
     * Monta a entidade a partir do modelo
     * @param Moloquent $model
     * @return bool
     */
    public function setAttributesWithModel (Moloquent $model): bool;
}