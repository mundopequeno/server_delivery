<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/03/18
 * Time: 19:28
 */

namespace App\Interfaces;


use App\Entities\Token;

interface TokenService
{
    /**
     * retorna a entidade token para manipular
     * @return Token
     */
    public function getEntitieToken ();
}