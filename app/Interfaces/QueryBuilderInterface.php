<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/02/2018
 * Time: 05:39
 */

namespace App\Interfaces;


use Illuminate\Http\Request;

interface QueryBuilderInterface
{
    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function show (Request $request);
}