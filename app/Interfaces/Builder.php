<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 13/01/2018
 * Time: 20:12
 */

namespace App\Interfaces;


interface Builder
{
    public function __construct($belongsToId, $objectWithValues, $status);
    public function save();
}