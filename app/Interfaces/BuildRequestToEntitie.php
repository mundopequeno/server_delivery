<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 14/03/18
 * Time: 04:57
 */

namespace App\Interfaces;

use \Illuminate\Http\Request;

interface BuildRequestToEntitie
{
    /**
     * Monta a entidade a partir da requisição
     * @param Request $request
     * @return void
     */
    public function setAttributesWithRequest (Request $request): void;
}