<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 01/03/2018
 * Time: 01:03
 */

namespace App\Interfaces;


use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

interface EntitieGenerateJSON
{
    /**
     * Este método é para retornar todos os atributos da classe
     * @return array
     */
    public function getAllAttributes (): array;
}