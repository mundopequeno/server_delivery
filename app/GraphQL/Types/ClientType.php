<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/02/2018
 * Time: 21:08
 */
class ClientType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Client',
        'description' => 'Um cliente'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'ID do cliente'
            ],
            'cpf' => [
                'type' => Type::string(),
                'description' => 'CPF do cliente'
            ]
        ];
    }
}