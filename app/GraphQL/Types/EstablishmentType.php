<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/02/2018
 * Time: 20:18
 */

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class EstablishmentType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Establishment',
        'description' => 'Um estabelecimento'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'ID do cliente'
            ],

            'nome' => [
                'type' => Type::string(),
                'description' => 'NOME do estabelecimento'
            ],
            'cnpj' => [
                'type' => Type::string(),
                'description' => 'CNPJ do estabelecimento'
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'E-MAIL do estabelecimento'
            ],
            'tipo' => [
                'type' => Type::string(),
                'description' => 'TIPO do estabelecimento'
            ],
            'horario_aberto' => [
                'type' => Type::string(),
                'description' => 'HORARIO_ABERTO do estabelecimento'
            ],
            'horario_fechado' => [
                'type' => Type::string(),
                'description' => 'HORARIO_FECHADO do estabelecimento'
            ],
            'status' => [
                'type' => Type::string(),
                'description' => 'STATUS do estabelecimento'
            ]
        ];
    }
}