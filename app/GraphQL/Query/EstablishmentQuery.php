<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/02/2018
 * Time: 20:23
 */

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Establishment;
class EstablishmentQuery extends Query
{
    protected $attributes = [
        'name' => 'establishments'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Establishment'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'nome' => ['name' => 'nome', 'type' => Type::string()],
            'cnpj' => ['name' => 'cnpj', 'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()],
            'tipo' => ['name' => 'tipo', 'type' => Type::string()],
            'horario_aberto' => ['name' => 'horario_aberto', 'type' => Type::string()],
            'horario_fechado' => ['name' => 'horario_fechado', 'type' => Type::string()],
            'status' => ['name' => 'status', 'type' => Type::string()],
        ];
    }

    public function resolve ($root, $args) {
        $keys = array_keys($args);

        $establishment = Establishment::where($keys[0], $keys[$keys[0]]);

        array_shift($args);

        foreach ($args as $key => $value)
            $establishment->where($key, $value);

        return $establishment->get();
    }
}