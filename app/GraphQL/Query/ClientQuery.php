<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 07/02/2018
 * Time: 21:17
 */

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Client;

class ClientQuery extends Query
{
    protected $attributes = [
        'name' => 'clients'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Client'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'cpf' => ['name' => 'cpf', 'type' => Type::string()]
        ];
    }

    public function resolve ($root, $args) {


        $data[] = [
            'id' => '123123123123',
            'cpf' => '312312312'
        ];
        return $data;

//        if (isset($args['id'])) {
//            return Client::find($args['id']);
//        } else if (isset($args['cpf'])) {
//            return Client::where('cpf', $args['cpf'])->get();
//        } else {
//            return Client::all();
//        }
    }
}